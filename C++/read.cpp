// ... include and using namespace
template < class T > 
inline void read ( T & x ) {
    char ch = getchar ( ); int f = 1;
    for ( ; ch < '0' || ch > '9' ; x = getchar ( ) ) f = ch == '-' ? -1 : 1;
    for ( ; ch >= '0' && ch <= '9'; x = getchar ( ) ) x = ( x << 3 ) + ( x << 1 ) + ( ch ^ 48 );
    x *= f;
}