@Override
	public void processEquipmentData(EquipmentData equipmentData) throws ServiceException {
		TVehicleDetector t = new TVehicleDetector();

		t.setEquipmentId(equipmentData.getEquipmentId());
		t.setEquipmentTypeCode(equipmentData.getEquipmentTypeCode());
		t.setUpdateTime(equipmentData.getReplyTime());
		String data =(String) equipmentData.getReplyBody().get("replyBody");
		JSONArray replyBody = JSONObject.parseObject(data.toString()).getJSONArray("data");
		if(replyBody.size() ==1 && "-99".equals(replyBody.getJSONObject(0).getString("lane"))){
				//平均车流
				t.setNumberFiled(replyBody.getJSONObject(0).getInteger("flow"));
		}else{
			//总车流量
			int sumFlux = 0;
			//总车道数
			int sumLane = replyBody.size();
			//平均车速
			double  averageSpeed = 0;
			for (int i = 0; i < replyBody.size(); i++) {
				JSONObject jsonObject = replyBody.getJSONObject(i);
				Map map = jsonObject.toJavaObject(Map.class);
				Set<Map.Entry<String, Object>> entry = map.entrySet();
				if (i==0) {
					for (Map.Entry<String, Object> entryStringEntry : entry) {

						if (entryStringEntry.getKey().equals("lane")) {
							//Logs.info(entryStringEntry.getKey());
							t.setCarWayOrder(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("wayNumber")) {
							t.setWayNumber(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("wayDirection  ")) {
							t.setWayDirection(entryStringEntry.getValue().toString());
						}

						if (entryStringEntry.getKey().equals("flow")) {
							sumFlux+=Integer.parseInt(entryStringEntry.getValue().toString());
							t.setFlux(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxOne")) {
							t.setFluxOne(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxTwo")) {
							t.setFluxTwo(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxThree")) {
							t.setFluxThree(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxFour")) {
							t.setFluxFour(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxFive")) {
							t.setFluxFive(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("rate")) {
							t.setOccupancy(Double.parseDouble(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("speed")) {
							averageSpeed+=Double.parseDouble(entryStringEntry.getValue().toString());
							t.setSpeed(Double.parseDouble(entryStringEntry.getValue().toString()));
						}
					}
				}
				if (i==1) {
					for (Map.Entry<String, Object> entryStringEntry : entry) {
						if (entryStringEntry.getKey().equals("flow")) {
							sumFlux+=Integer.parseInt(entryStringEntry.getValue().toString());
							t.setTwoFlux(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxOne")) {
							t.setTwoFluxOne(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxTwo")) {
							t.setTwoFluxTwo(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxThree")) {
							t.setTwoFluxThree(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxFour")) {
							t.setTwoFluxFour(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxFive")) {
							t.setTwoFluxFive(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("rate")) {
							t.setTwoOccupancy(Double.parseDouble(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("speed")) {
							averageSpeed+=Double.parseDouble(entryStringEntry.getValue().toString());
							t.setTwoSpeed(Double.parseDouble(entryStringEntry.getValue().toString()));
						}
					}
				}

				if (i==2) {
					for (Map.Entry<String, Object> entryStringEntry : entry) {
						if (entryStringEntry.getKey().equals("flow")) {
							sumFlux+=Integer.parseInt(entryStringEntry.getValue().toString());
							t.setThreeFlux(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxOne")) {
							t.setThreeFluxOne(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxTwo")) {
							t.setThreeFluxTwo(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxThree")) {
							t.setThreeFluxThree(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxFour")) {
							t.setThreeFluxFour(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxFive")) {
							t.setThreeFluxFive(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("rate")) {
							t.setThreeOccupancy(Double.parseDouble(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("speed")) {
							averageSpeed+=Double.parseDouble(entryStringEntry.getValue().toString());
							t.setThreeThreeSpeed(Double.parseDouble(entryStringEntry.getValue().toString()));
						}
					}
				}

				if (i==3) {
					for (Map.Entry<String, Object> entryStringEntry : entry) {
						if (entryStringEntry.getKey().equals("flow")) {
							sumFlux+=Integer.parseInt(entryStringEntry.getValue().toString());
							t.setFourFlux(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxOne")) {
							t.setFourFluxOne(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxTwo")) {
							t.setFourFluxTwo(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxThree")) {
							t.setFourFluxThree(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxFour")) {
							t.setFourFluxFour(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxFive")) {
							t.setFourFluxFive(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("rate")) {
							t.setFourOccupancy(Double.parseDouble(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("speed")) {
							averageSpeed+=Double.parseDouble(entryStringEntry.getValue().toString());
							t.setFourSpeed(Double.parseDouble(entryStringEntry.getValue().toString()));
						}
					}
				}

				if (i==4) {
					for (Map.Entry<String, Object> entryStringEntry : entry) {
						if (entryStringEntry.getKey().equals("flow")) {
							sumFlux+=Integer.parseInt(entryStringEntry.getValue().toString());
							t.setFiveFlux(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxOne")) {
							t.setFiveFluxOne(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxTwo")) {
							t.setFiveFluxTwo(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxThree")) {
							t.setFiveFluxThree(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxFour")) {
							t.setFiveFluxFour(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxFive")) {
							t.setFiveFluxFive(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("rate")) {
							t.setFiveOccupancy(Double.parseDouble(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("speed")) {
							averageSpeed+=Double.parseDouble(entryStringEntry.getValue().toString());
							t.setFiveSpeed(Double.parseDouble(entryStringEntry.getValue().toString()));
						}
					}
				}

				if (i==5) {
					for (Map.Entry<String, Object> entryStringEntry : entry) {
						if (entryStringEntry.getKey().equals("flow")) {
							sumFlux+=Integer.parseInt(entryStringEntry.getValue().toString());
							t.setSixFlux(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxOne")) {
							t.setSixFluxOne(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxTwo")) {
							t.setSixFluxTwo(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxThree")) {
							t.setSixFluxThree(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxFour")) {
							t.setSixFluxFour(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("fluxFive")) {
							t.setSixFluxFive(Integer.parseInt(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("rate")) {
							t.setSixOccupancy(Double.parseDouble(entryStringEntry.getValue().toString()));
						}
						if (entryStringEntry.getKey().equals("speed")) {
							averageSpeed+=Double.parseDouble(entryStringEntry.getValue().toString());
							t.setSixSpeed(Double.parseDouble(entryStringEntry.getValue().toString()));
						}
					}
				}
			}

			t.setSumFluex(sumFlux);
			t.setAverageSpeed(averageSpeed/sumLane);
		}
		// 如果总车流量低于20，则调整左洞/右洞的照明亮度至20。如果不是就恢复默认值
		reduceLighting.reduceLighting(t);
		//插入数据
		this.iVehicleDetectorService.saveOrUpdate(t);

		//保存历史数据
		TEmVehicleDetectorHistory history =  JSONObject.parseObject(JSONObject.toJSONString(t), TEmVehicleDetectorHistory.class);
		vehicleDetectorHistoryService.save(history);
	}
