import java.util.Arrays;
import java.util.Random;

/**
 * 猴子排序
 */
public class MonkeySort {

    public static void main(String[] args) {
        int[] arr = {5, 2, 8, 6, 1, 3, 9, 4, 7}; // 待排序的数组
        monkeySort(arr);
        System.out.println(Arrays.toString(arr));
    }

    /**
     * 猴子排序
     * @param arr
     */
    public static void monkeySort(int[] arr) {
        Random random = new Random();
        while (!isSorted(arr)) {
            shuffle(arr, random);
        }
    }

    /**
     * 判断是否是有序的数组
     * @param arr
     * @return
     */
    public static boolean isSorted(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                return false;
            }
        }
        return true;
    }

    /**
     * 随机排序
     * @param arr
     * @param random
     */
    public static void shuffle(int[] arr, Random random) {
        for (int i = 0; i < arr.length; i++) {
            int j = random.nextInt(arr.length);
            swap(arr, i, j);
        }
    }

    /**
     * 交换位置
     * @param arr
     * @param i
     * @param j
     */
    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}