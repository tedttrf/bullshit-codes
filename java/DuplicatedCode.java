package com.data.core;

import com.data.core.mysql.SaveDataDao;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@Component
public class DuplicatedCode {

    @Resource
    private SaveDataDao saveDataDao;

     
    /**
     * 保存各个景区均值数据
     *   
     * 为了应付公司每月统计代码行数，同事的写法也是醉了...
     */
    public void scenicPassengerFlowVolumnData(){
        Calendar c = Calendar.getInstance();
        String marineaquariumNo="0";
        String internationalTouristResortNo="0";
        String  wildlifeparkNo="0";
        String orientalpearltowerNo="0";
        String haichangoceanparkNo="0";
        String shanghaiScienceTechnologyMuseumNo="0";
        String centuryparkNo="0";
        String jinmaoNo="0";
        String huanqiuNo="0";
        String baolanzhongxinNo="0";
        String xianhuagangNo="0";
        String xinchangguzhenNo = "0";
        String shanghaizhidianNo = "0";
        String hanghaiNo = "0";
        String xunyicaoNo = "0";
        String zhoupuhuahaiNo = "0";
        String binhaislgyNo = "0";
        String zhongyiyaobwgNo = "0";

        /**
         *  省略业务代码
         */

        // 海洋水族馆
        Map<String, Object> marineaquarium = new HashMap<>();
        marineaquarium.put("insertdate", c.getTime());
        marineaquarium.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        marineaquarium.put("total",marineaquariumNo);
        marineaquarium.put("id", UUID.randomUUID().toString());
        marineaquarium.put("businesstype", "均值");
        saveDataDao.insertData("t_marineaquarium", marineaquarium);
        // 国际旅游度假区
        Map<String, Object> internationalTouristResort = new HashMap<>();
        internationalTouristResort.put("insertdate", c.getTime());
        internationalTouristResort.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        internationalTouristResort.put("total",internationalTouristResortNo);
        internationalTouristResort.put("id", UUID.randomUUID().toString());
        internationalTouristResort.put("businesstype", "均值");
        saveDataDao.insertData("t_internationaltouristresort", internationalTouristResort);
        // 野生动物园
        Map<String, Object> wildlifepark = new HashMap<>();
        wildlifepark.put("insertdate", c.getTime());
        wildlifepark.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        wildlifepark.put("total",wildlifeparkNo);
        wildlifepark.put("id", UUID.randomUUID().toString());
        wildlifepark.put("businesstype", "均值");
        saveDataDao.insertData("t_wildlifepark", wildlifepark);
        // 东方明珠塔
        Map<String, Object> orientalpearltower = new HashMap<>();
        orientalpearltower.put("insertdate", c.getTime());
        orientalpearltower.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        orientalpearltower.put("total",orientalpearltowerNo);
        orientalpearltower.put("id", UUID.randomUUID().toString());
        orientalpearltower.put("businesstype", "均值");
        saveDataDao.insertData("t_orientalpearltower", orientalpearltower);
        // 海昌海洋公园
        Map<String, Object> haichangoceanpark = new HashMap<>();
        haichangoceanpark.put("insertdate", c.getTime());
        haichangoceanpark.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        haichangoceanpark.put("total",haichangoceanparkNo);
        haichangoceanpark.put("id", UUID.randomUUID().toString());
        haichangoceanpark.put("businesstype", "均值");
        saveDataDao.insertData("t_haichangoceanpark", haichangoceanpark);
        // 上海科技馆
        Map<String, Object> ShanghaiScienceTechnologyMuseum = new HashMap<>();
        ShanghaiScienceTechnologyMuseum.put("insertdate", c.getTime());
        ShanghaiScienceTechnologyMuseum.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        ShanghaiScienceTechnologyMuseum.put("total",shanghaiScienceTechnologyMuseumNo);
        ShanghaiScienceTechnologyMuseum.put("id", UUID.randomUUID().toString());
        ShanghaiScienceTechnologyMuseum.put("businesstype", "均值");
        saveDataDao.insertData("t_shanghaiscienceTechnologymuseum", ShanghaiScienceTechnologyMuseum);
        // 世纪公园
        Map<String, Object> centurypark = new HashMap<>();
        centurypark.put("insertdate", c.getTime());
        centurypark.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        centurypark.put("total",centuryparkNo);
        centurypark.put("id", UUID.randomUUID().toString());
        centurypark.put("businesstype", "均值");
        saveDataDao.insertData("t_centurypark", centurypark);
        // 金茂
        Map<String, Object> jinmaoMap = new HashMap<>();
        jinmaoMap.put("insertdate", c.getTime());
        jinmaoMap.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        jinmaoMap.put("total",jinmaoNo);
        jinmaoMap.put("id", UUID.randomUUID().toString());
        jinmaoMap.put("businesstype", "均值");
        saveDataDao.insertData("t_jinmao", jinmaoMap);
        // 环球
        Map<String, Object> huanqiuMap = new HashMap<>();
        huanqiuMap.put("insertdate", c.getTime());
        huanqiuMap.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        huanqiuMap.put("total",huanqiuNo);
        huanqiuMap.put("id", UUID.randomUUID().toString());
        huanqiuMap.put("businesstype", "均值");
        saveDataDao.insertData("t_huanqiu", huanqiuMap);
        // 上海国际博览中心
        Map<String, Object> baolanzhongxinMap = new HashMap<>();
        baolanzhongxinMap.put("insertdate", c.getTime());
        baolanzhongxinMap.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        baolanzhongxinMap.put("total",baolanzhongxinNo);
        baolanzhongxinMap.put("id", UUID.randomUUID().toString());
        baolanzhongxinMap.put("businesstype", "均值");
        saveDataDao.insertData("t_bolanzhongxin", baolanzhongxinMap);
        // 上海鲜花港
        Map<String, Object> xianhuagangMap = new HashMap<>();
        xianhuagangMap.put("insertdate", c.getTime());
        xianhuagangMap.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        xianhuagangMap.put("total",xianhuagangNo);
        xianhuagangMap.put("id", UUID.randomUUID().toString());
        xianhuagangMap.put("businesstype", "均值");
        saveDataDao.insertData("t_xianhuagang", xianhuagangMap);
        // 新场古镇
        Map<String, Object> xinchangguzhenMap = new HashMap<>();
        xinchangguzhenMap.put("insertdate", c.getTime());
        xinchangguzhenMap.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        xinchangguzhenMap.put("total",xinchangguzhenNo);
        xinchangguzhenMap.put("id", UUID.randomUUID().toString());
        xinchangguzhenMap.put("businesstype", "均值");
        saveDataDao.insertData("t_xinchangguzhen", xinchangguzhenMap);
        // 上海之巅观光厅
        Map<String, Object> shanghaizhidianMap = new HashMap<>();
        shanghaizhidianMap.put("insertdate", c.getTime());
        shanghaizhidianMap.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        shanghaizhidianMap.put("total",shanghaizhidianNo);
        shanghaizhidianMap.put("id", UUID.randomUUID().toString());
        shanghaizhidianMap.put("businesstype", "均值");
        saveDataDao.insertData("t_shanghaizhidian", shanghaizhidianMap);
        // 航海博物馆
        Map<String, Object> hanghaiMap = new HashMap<>();
        hanghaiMap.put("insertdate", c.getTime());
        hanghaiMap.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        hanghaiMap.put("total",hanghaiNo);
        hanghaiMap.put("id", UUID.randomUUID().toString());
        hanghaiMap.put("businesstype", "均值");
        saveDataDao.insertData("t_hanghaibowuguan", hanghaiMap);
        // 薰衣草公园
        Map<String, Object> xunyicaoMap = new HashMap<>();
        xunyicaoMap.put("insertdate", c.getTime());
        xunyicaoMap.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        xunyicaoMap.put("total",xunyicaoNo);
        xunyicaoMap.put("id", UUID.randomUUID().toString());
        xunyicaoMap.put("businesstype", "均值");
        saveDataDao.insertData("t_xunyicaogongyuan", xunyicaoMap);
        // 周浦花海
        Map<String, Object> zhoupuhuaMap = new HashMap<>();
        zhoupuhuaMap.put("insertdate", c.getTime());
        zhoupuhuaMap.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        zhoupuhuaMap.put("total",zhoupuhuahaiNo);
        zhoupuhuaMap.put("id", UUID.randomUUID().toString());
        zhoupuhuaMap.put("businesstype", "均值");
        saveDataDao.insertData("t_zhoupuhuahai", zhoupuhuaMap);
        // 滨海森林公园
        Map<String, Object> binhaislMap = new HashMap<>();
        binhaislMap.put("insertdate", c.getTime());
        binhaislMap.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        binhaislMap.put("total",binhaislgyNo);
        binhaislMap.put("id", UUID.randomUUID().toString());
        binhaislMap.put("businesstype", "均值");
        saveDataDao.insertData("t_binhaisenlingongyuan", binhaislMap);
        // 上海中医药博物馆
        Map<String, Object> zhongyiyaobwgMap = new HashMap<>();
        zhongyiyaobwgMap.put("insertdate", c.getTime());
        zhongyiyaobwgMap.put("xval",(c.get(Calendar.HOUR_OF_DAY)+1) + "时");
        zhongyiyaobwgMap.put("total",zhongyiyaobwgNo);
        zhongyiyaobwgMap.put("id", UUID.randomUUID().toString());
        zhongyiyaobwgMap.put("businesstype", "均值");
        saveDataDao.insertData("t_zhongyiyaobowuguan", zhongyiyaobwgMap);
    }

}
