package test;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

/**
 * 别问我为什么用String做类名，这又何尝不是shit-code
 *
 * @author qp
 * @date 2023/7/13 11:38
 */
public class String {


    /**
     * 全字符串拼接的含金量，懂？
     * <p>
     * 你以为这是一个demo，但是这样的写法占了我接手的大半个项目，
     * 改成实体搞了我好几天🤡👈
     */
    public static void main(java.lang.String[] args) {

        LocalDateTime now = LocalDateTime.now();
        java.lang.String active = "1.0.0";
        int bizType = 2;
        java.lang.String body = "version ：" + active + "\n时间：" + now + "\n " + bizType + "\n ";

        java.lang.String[] strings = {
                "111",
                "222",
        };
        int length = strings.length;

        int i = RandomUtils.nextInt(0, length);

        RestTemplate restTemplate = new RestTemplate();
        java.lang.String json22 = "{\"msgtype\": \"text\", " +
                "        \"text\": {" +
                "             \"content\": \"" + body + "\"" +
                "        }" +
                "     }";
        restTemplate.postForObject("url" + strings[i], JSONObject.parse(json22), java.lang.String.class);
    }
}
