
```
例如 params[:key] 传入一个 字符串为 'a'
在方法中 已有hash = {a: '这是A', b: '这是B'}
取值：
JSON.parse(hash.to_json)[params[:key]]
```
可以类似修改如下：
```
hash[params[:key].to_sym]

```